"""
FashionClassifier - Attempt 3
Used the MINIST fashion dataset to classify images of clothing
By Katelyn Hamer

https://www.tensorflow.org/tutorials/keras/classification
Things I need to go over later are marked with LEARN:
"""

import logging
from logging import handlers
from pathlib import Path
from typing import Iterable, Union
import datetime
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import click

LOG_BASE_FILENAME = "clothing_classifier_attempt3.log"  # Base name of log file
USE_EXISTING_MODEL = True  # If enabled and a model exists on the filesystem, it will be loaded
MODEL_PATH = Path("clothing_classifier")  # Where model will be loaded from
MATCH_THRESHOLD = 0.5  # Predictions with probabilities greater than this are considered 'good'
CLASS_NAMES = ["T-shirt/top", "Trouser", "Pullover", "Dress",
               "Coat", "Sandal", "Shirt", "Sneaker", "Bag", "Boot"]  # NOTE: Ankle Boot renamed to Boot for brevity


# Set up logging
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.StreamHandler(),
        handlers.RotatingFileHandler(
            # 250 KB
            LOG_BASE_FILENAME,
            maxBytes=(25 * 10000),
            backupCount=7,
        ),
    ],
)

logging.getLogger("matplotlib").setLevel(
    logging.ERROR)  # Disable matplotlib debugging


def load_dataset():
    """
    Load the MINIST fashion dataset
    Returns: (tuple) training images, training labels
             (tuple) testing images, testing labels
    """
    fashion_mnist = tf.keras.datasets.fashion_mnist
    (train_images, train_labels), (test_images,
                                   test_labels) = fashion_mnist.load_data()

    #train_images = train_images / 255.0  # LEARN: Why divide these by 255? 
    #test_images = test_images / 255.0
    # The reason we divide these by 255 is to normalise their values to between 0 and 1,
    # we do this because this is the type of data we will be using with our model
    return (train_images, train_labels), (test_images, test_labels)

def create_model():
    """Creates an empty model"""
    # LEARN: How models are structured and what each layer does
    return tf.keras.Sequential([
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        tf.keras.layers.experimental.preprocessing.Rescaling(1./255, input_shape=(28, 28)),
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dropout(0.1),
        tf.keras.layers.Dense(10)
    ])


def compile_and_train_model(model, train_images, train_labels):
    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(
                      from_logits=True),
                  metrics=['accuracy'])
    model.fit(train_images, train_labels, epochs=10)
    return model


def predict(model, images):
    """
    Use the model to predict the probability of a classification
    This is a wrapper function around model.predict that creates a probability model
    Arguments: images, a numpy array of images to predict on, can be a single image
    Returns: a numpy array of predictions
    """
    # LEARN: Why this is neccesary and how it works
    probability_model = tf.keras.Sequential([model,
                                             tf.keras.layers.Softmax()])
    predictions = probability_model.predict(images)
    return predictions


def test_model(model, test_images, test_labels, visual_output=True):
    """Evaluate the model accuracy"""
    # TODO add graphs and visual testing output
    test_loss, test_acc = model.evaluate(
        test_images,  test_labels, verbose=2)
    logging.debug(f"Model accuracy on test set: {test_acc}")

    logging.debug("Testing model with visual output...")
    if visual_output:  # Enable matplotlib graphing of testing
        # Test 25 images from the test dataset
        test_images_small, test_labels_small = test_images[:25], test_labels[:25]
        test_results = []  # Test results are stored here to be printed later
        predictions = predict(model, test_images_small)

        # Initialise matplotlib figure for later
        plt.figure()
        plt.xticks([])
        plt.yticks([])
        plt.subplots_adjust(hspace=0.9)
        plt.grid(True)
        plt.suptitle("Tensorflow Clothing Classifier Test")

        for index, prediction in enumerate(predictions):
            prediction_result = np.argmax(prediction)  # Get the class with the highest probability
            prediction_probability = prediction[prediction_result]  # Get the probability of the prediction
            prediction_class_name = CLASS_NAMES[prediction_result]  # Get the predicted class name

            actual_result = test_labels[index]  # Get the actual label of the image
            actual_class_name = CLASS_NAMES[actual_result]  # Get the actual class name

            prediction_was_correct = bool(prediction_result == actual_result)
            log_color = "green" if prediction_was_correct else "red"
            log_message = f"#{index}: {prediction_class_name} ({prediction_result}) ({prediction_probability * 100}%) -> Was {actual_class_name} ({actual_result})"
            if not prediction_probability >= MATCH_THRESHOLD:
                log_message += " (unsure)"
            logging.debug(click.style(log_message, fg=log_color))  # Log the test message with color
            
            # Plot the results onto a matplotlib figure
            # TODO: tidy up matplotlib code
            axis = plt.subplot(5, 5, index + 1)
            plt.imshow(test_images_small[index])
            axis.set_xlabel(f"{prediction_class_name}\n{round(prediction_probability * 100, 2)}%",
            color=log_color)
            axis.set_xticks([])
            axis.set_yticks([])

        plt.show()

    return test_loss, test_acc

if __name__ == "__main__":
    logging.debug("== Starting ClothingClassifier ==")  # Allows us to delineate logs more easily
    try:
        logging.debug("Loading MINIST fashion dataset...")
        train_dataset, test_dataset = load_dataset()
        train_images, train_labels = train_dataset
        test_images, test_labels = test_dataset
        foreign_dataset = tf.keras.preprocessing.image_dataset_from_directory("foreign_data", labels='inferred', image_size=(28, 28), color_mode='grayscale')
        for images, labels in foreign_dataset.take(1):  # this is irritating
            foreign_labels = labels.numpy()
            foreign_images = images.numpy()

        """
        This can help with solid backgrounds getting in the way of correct classification
        We replace any pixels that are close to pure white with dark pixels
        This can occassionaly break otherwise easily classifiable images and
        doesn't work with images with backgrounds of colors other than solid white
        In the future, I'd like to replace this with either another neural network
        doing background removal, or an edge detection algorithm.
        """
        for image in foreign_images:
            image[image > 230] = 3

        if USE_EXISTING_MODEL and MODEL_PATH.exists():
            model_created_time = datetime.datetime.fromtimestamp(MODEL_PATH.stat().st_mtime).strftime("%c")
            logging.debug(f"Using existing model in {MODEL_PATH} (created at {model_created_time})")
            model = tf.keras.models.load_model(MODEL_PATH)
        else:
            logging.debug("Creating new model...")
            model = create_model()
            logging.debug("Compiling and training new model...")
            compile_and_train_model(model, train_images, train_labels)
            logging.debug(f"Saving model to {MODEL_PATH}")
            model.save(MODEL_PATH)

        logging.debug("Evaluating model...")
        test_model(model, foreign_images, foreign_labels)
        test_model(model, test_images[:25], test_labels[:25])
    except Exception as exc:  # We want to log all exceptions
        logging.exception(exc)
        exit(-1)
    finally:
        logging.debug("== Done ==")