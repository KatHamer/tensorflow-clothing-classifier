# Tensorflow Clothing Classifier - Attempt 3

This is my third attempt at creating a clothing classifier using Tensorflow. I am using this project in order to learn about machine learning and datascience, and to become familiar with Tensorflow, Keras, Matplotlib, etc.

## Code

The source code for this project may be messy, incorrect or incomplete in places. Sections of code may be taken from tutorials (linked in docstrings). Lines or sections I don't fully understand are commented with the designation, 'LEARN:' so that I can go back and reinforce my learning later.

## Branches

Each branch of this project is a different working state. Check them out to see the different stages of the project, they each have their own README and attached screenshots.

